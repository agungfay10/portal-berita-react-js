import './App.css';
import News from './PortalBerita';

function App() {
  return (
    <div className="App">
      <News />
    </div>
  );
}

export default App;
