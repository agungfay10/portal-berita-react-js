import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import axios from "axios";
import { MagnifyingGlass } from "react-loader-spinner";

export default class News extends React.Component {
  state = {
    articles: [],
    allArticles: [],
    isLoading: true,
    errors: null,
  };

  componentDidMount() {
    axios
      .get(
        "https://newsapi.org/v2/everything?domains=liputan6.com,tribunnews.com,detik.com&apiKey=f592cc399956459d8420183485fc80a9"
        // "https://newsapi.org/v2/top-headlines?country=id&apiKey=f592cc399956459d8420183485fc80a9"
      )
      .then((response) =>
        response.data.articles.map((article) => ({
          img: `${article.urlToImage}`,
          title: `${article.title}`,
          desc: `${article.description}`,
          author: `${article.author}`,
          date: `${article.publishedAt}`,
          url: `${article.url}`,
        }))
      )
      .then((article) => {
        this.setState({
          articles: article,
          allArticles: article,
          isLoading: false,
        });
      })
      .catch((error) => this.setState({ error, isLoading: false }));
  }

  _onKeyUp = (e) => {
    const articles = this.state.allArticles.filter((item) =>
      item.title.toLowerCase().includes(e.target.value.toLowerCase())
    );
    this.setState({articles});
  };

  render() {
    const { isLoading, articles } = this.state;
    return (
      <div>
        <Navbar bg="success" variant="dark" expand="lg">
          <Container>
            <Navbar.Brand>React Class News</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link href="">Home</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>

        <Container>
          <Form className="d-flex mt-4">
            <Form.Control
              type="search"
              onChange={this._onKeyUp}
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search Newss</Button>
          </Form>
        </Container>

        <Container>
          <div className="d-flex justify-content-around row mt-4">
            {!isLoading ? (
              articles.map((article) => {
                const { img, desc, author, date, title, url } = article;
                var tgl = date.split("T");
                return (
                  <div
                    className="card mt-4"
                    style={{ width: "22rem" }}
                    key={title}
                  >
                    <img src={img} className="card-img-top" alt=" "></img>
                    <div className="card-body">
                      <h5 className="card-title">{title}</h5>
                      <h6 className="text-secondary">
                        {author} - {tgl[0]} {tgl[1]}
                      </h6>
                      <p className="card-text">{desc}</p>
                      <Button href={url} variant="success">
                        Details
                      </Button>
                    </div>
                  </div>
                );
              })
            ) : (
              <MagnifyingGlass
                visible={true}
                height="80"
                width="80"
                ariaLabel="MagnifyingGlass-loading"
                wrapperStyle={{}}
                wrapperClass="MagnifyingGlass-wrapper"
                glassColor="#c0efff"
                color="#198754"
              />
            )}
          </div>
        </Container>
      </div>
    );
  }
}
